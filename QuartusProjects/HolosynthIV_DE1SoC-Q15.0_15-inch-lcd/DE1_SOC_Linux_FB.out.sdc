## Generated SDC file "DE1_SOC_Linux_FB.out.sdc"

## Copyright (C) 2017  Intel Corporation. All rights reserved.
## Your use of Intel Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Intel Program License 
## Subscription Agreement, the Intel Quartus Prime License Agreement,
## the Intel MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Intel and sold by Intel or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 17.0.1 Build 598 06/07/2017 SJ Standard Edition"

## DATE    "Mon Jul 10 19:39:43 2017"

##
## DEVICE  "5CSEMA5F31C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {clock_50} -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLOCK_50}]
create_clock -name {clock3_50} -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLOCK3_50}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {soc_system:u0|soc_system_pll_stream:pll_stream|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} -source [get_pins {u0|pll_stream|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|refclkin}] -duty_cycle 50/1 -multiply_by 12 -divide_by 2 -master_clock {clock_50} [get_pins {u0|pll_stream|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0]}] 
create_generated_clock -name {soc_system:u0|soc_system_pll_stream:pll_stream|altera_pll:altera_pll_i|outclk_wire[0]} -source [get_pins {u0|pll_stream|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}] -duty_cycle 50/1 -multiply_by 1 -divide_by 2 -master_clock {soc_system:u0|soc_system_pll_stream:pll_stream|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} [get_pins {u0|pll_stream|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 
create_generated_clock -name {soc_system:u0|soc_system_pll_stream:pll_stream|altera_pll:altera_pll_i|outclk_wire[1]} -source [get_pins {u0|pll_stream|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}] -duty_cycle 50/1 -multiply_by 1 -divide_by 4 -master_clock {soc_system:u0|soc_system_pll_stream:pll_stream|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} [get_pins {u0|pll_stream|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk}] 
create_generated_clock -name {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} -source [get_pins {u0|pll_audio|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|refclkin}] -duty_cycle 50/1 -multiply_by 58 -divide_by 2 -master_clock {clock_50} [get_pins {u0|pll_audio|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0]}] 
create_generated_clock -name {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|outclk_wire[0]} -source [get_pins {u0|pll_audio|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}] -duty_cycle 50/1 -multiply_by 1 -divide_by 59 -master_clock {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} [get_pins {u0|pll_audio|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 
create_generated_clock -name {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|outclk_wire[1]} -source [get_pins {u0|pll_audio|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}] -duty_cycle 50/1 -multiply_by 1 -divide_by 43 -master_clock {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} [get_pins {u0|pll_audio|altera_pll_i|general[1].gpll~PLL_OUTPUT_COUNTER|divclk}] 
create_generated_clock -name {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|outclk_wire[2]} -source [get_pins {u0|pll_audio|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}] -duty_cycle 50/1 -multiply_by 1 -divide_by 16 -master_clock {soc_system:u0|soc_system_pll_audio:pll_audio|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} [get_pins {u0|pll_audio|altera_pll_i|general[2].gpll~PLL_OUTPUT_COUNTER|divclk}] 
create_generated_clock -name {synthesizer:synthesizer_inst|sys_pll:sys_disp_pll_inst|sys_pll_0002:sys_pll_inst|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} -source [get_pins {synthesizer_inst|sys_disp_pll_inst|sys_pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|refclkin}] -duty_cycle 50/1 -multiply_by 12 -divide_by 2 -master_clock {clock_50} [get_pins {synthesizer_inst|sys_disp_pll_inst|sys_pll_inst|altera_pll_i|general[0].gpll~FRACTIONAL_PLL|vcoph[0]}] 
create_generated_clock -name {synthesizer:synthesizer_inst|sys_pll:sys_disp_pll_inst|sys_pll_0002:sys_pll_inst|altera_pll:altera_pll_i|outclk_wire[0]} -source [get_pins {synthesizer_inst|sys_disp_pll_inst|sys_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|vco0ph[0]}] -duty_cycle 50/1 -multiply_by 1 -divide_by 12 -master_clock {synthesizer:synthesizer_inst|sys_pll:sys_disp_pll_inst|sys_pll_0002:sys_pll_inst|altera_pll:altera_pll_i|general[0].gpll~FRACTIONAL_PLL_O_VCOPH0} [get_pins {synthesizer_inst|sys_disp_pll_inst|sys_pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {clock3_50}] -rise_to [get_clocks {clock3_50}] -setup 0.310  
set_clock_uncertainty -rise_from [get_clocks {clock3_50}] -rise_to [get_clocks {clock3_50}] -hold 0.270  
set_clock_uncertainty -rise_from [get_clocks {clock3_50}] -fall_to [get_clocks {clock3_50}] -setup 0.310  
set_clock_uncertainty -rise_from [get_clocks {clock3_50}] -fall_to [get_clocks {clock3_50}] -hold 0.270  
set_clock_uncertainty -rise_from [get_clocks {clock3_50}] -rise_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -rise_from [get_clocks {clock3_50}] -fall_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -fall_from [get_clocks {clock3_50}] -rise_to [get_clocks {clock3_50}] -setup 0.310  
set_clock_uncertainty -fall_from [get_clocks {clock3_50}] -rise_to [get_clocks {clock3_50}] -hold 0.270  
set_clock_uncertainty -fall_from [get_clocks {clock3_50}] -fall_to [get_clocks {clock3_50}] -setup 0.310  
set_clock_uncertainty -fall_from [get_clocks {clock3_50}] -fall_to [get_clocks {clock3_50}] -hold 0.270  
set_clock_uncertainty -fall_from [get_clocks {clock3_50}] -rise_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -fall_from [get_clocks {clock3_50}] -fall_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -rise_from [get_clocks {clock_50}] -rise_to [get_clocks {clock3_50}]  0.330  
set_clock_uncertainty -rise_from [get_clocks {clock_50}] -fall_to [get_clocks {clock3_50}]  0.330  
set_clock_uncertainty -rise_from [get_clocks {clock_50}] -rise_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -rise_from [get_clocks {clock_50}] -fall_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -fall_from [get_clocks {clock_50}] -rise_to [get_clocks {clock3_50}]  0.330  
set_clock_uncertainty -fall_from [get_clocks {clock_50}] -fall_to [get_clocks {clock3_50}]  0.330  
set_clock_uncertainty -fall_from [get_clocks {clock_50}] -rise_to [get_clocks {clock_50}]  0.330  
set_clock_uncertainty -fall_from [get_clocks {clock_50}] -fall_to [get_clocks {clock_50}]  0.330  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

